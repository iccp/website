const path = require("path")

module.exports = {
  siteMetadata: {
    title: `International Childrens Care Phillipines`,
    description: `ICCP provides holistic care for orphaned, abandoned, and destitute children.`,
    author: `@whmountains`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, `src`, `images`)
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `International Childrens Care Phillippines`,
        short_name: `iccp`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#667eea`,
        display: `minimal-ui`,
        icon: `src/images/app-icon.png`
      }
    },
    `gatsby-plugin-postcss`,
    {
      resolve: "gatsby-plugin-purgecss",
      options: {
        tailwind: true,
        purgeOnly: ["src/css/style.css"]
      }
    }
  ]
}
